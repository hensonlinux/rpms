%define SHA256SUM0 97f2b7c6b06d29718186251a76fec048583a431f3d88742f9f575926cef7a49b
%undefine _disable_source_fetch
Name:		console-tdm
Version:	1.3.1
Release:	3%{?dist}
Summary:	console-tdm
Source:		https://github.com/dopsi/%{name}/archive/v%{version}.tar.gz
Source1:	zzz-tdm.sh
License:	GPL v3.0
Prefix:		/usr/local
URL:		https://github.com/dopsi/console-tdm

Requires:	bash, xorg-x11-xinit

%description
Console TDM is an extension for xorg-xinit written in pure bash. It is inspired by CDM, which aimed to be a replacement of display managers such as GDM. 

%prep
echo "%SHA256SUM0 %SOURCE0" | sha256sum -c -
%setup -q

%install
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}/etc/profile.d
cp %{SOURCE1} %{buildroot}/etc/profile.d
install %{SOURCE1} %{buildroot}%{_sysconfdir}/profile.d

%files
/etc/profile.d/zzz-tdm.sh
/usr/local/bin/tdm
/usr/local/bin/tdmctl
/usr/local/share/bash-completion/completions/tdmctl
/usr/local/share/man/man1/tdm.1
/usr/local/share/man/man1/tdmctl.1
/usr/local/share/tdm/sessions/E17
/usr/local/share/tdm/sessions/Fluxbox
/usr/local/share/tdm/sessions/GNOME
/usr/local/share/tdm/sessions/ICEWM
/usr/local/share/tdm/sessions/PekWM
/usr/local/share/tdm/sessions/XFCE4
/usr/local/share/tdm/sessions/openbox
/usr/local/share/tdm/sessions/xmonad
/usr/local/share/tdm/tdmexit
/usr/local/share/tdm/tdminit
/usr/local/share/zsh/site-functions/_tdmctl

%changelog

